Pour utiliser le framework, il suffit de récupérer le dossier framework et l'inclure dans un projet HTML. Dans la page HTML, la feuille de style intégrée sera le fichier framework.css du dossier /framework.

Exemple d'utilisation:
  -Création d'une alerte avertissement: dans le fichier HTML nommer la classe "btn-avertissement".
  -Création d'une liste avec un square: appeler le nom de classe de l'ul correspondante : "square".

Pour l'utilisation d'une grille:
  - il faut faire appel au mixin de construction de la grille au début du fichier CSS (saisie de la largeur de l'écran, nombre de colonne désirée, taille goulotte et marges).
  -Les différentes classes sont générées automatiquement. Par exemple, pour un élément de taille 5 colonnes, il suffit de nommer dans l'HTML la classe avec le nom "col5".Pareillement, pour une marge gauche de 2colonnes, il suffit de nommer en plus la classe "off2".